from __future__ import annotations

import itertools
import math
import operator
import statistics
from collections.abc import Iterable
from dataclasses import dataclass, field
from typing import TypeVar

from pyglet.math import Vec2

RESISTANCE = 3

ZERO = Vec2(0, 0)


@dataclass
class Circle:
    radius: float
    mass: float
    restitution: float
    pos: Vec2
    vel: Vec2 = field(default_factory=lambda: ZERO)

    def vel_in(self, time: float) -> Vec2:
        if self.vel == ZERO:
            return self.vel
        u = abs(self.vel)
        return self.vel.from_magnitude(u * math.exp(-RESISTANCE * time))

    def pos_in(self, time: float) -> Vec2:
        if self.vel == ZERO:
            return self.pos
        u = abs(self.vel)
        dist_travelled = u / RESISTANCE * (1 - math.exp(-RESISTANCE * time))
        return self.pos + self.vel.from_magnitude(dist_travelled)

    def update(self, time: float) -> None:
        self.pos = self.pos_in(time)
        self.vel = self.vel_in(time)


@dataclass
class Event:
    time_until: float


T = TypeVar("T", bound=Event)


def soonest(iterable: Iterable[T], /) -> T | None:
    try:
        return min(iterable, key=operator.attrgetter("time_until"))
    except ValueError:
        return None


@dataclass
class Collision(Event):
    participants: tuple[Circle, Circle]
    impact_speed: float | None = None


def time_to_go(distance: float, speed: float) -> float:
    return -1 / RESISTANCE * math.log(1 - distance * RESISTANCE / speed)


def find_first_collision(spheres: Iterable[Circle]) -> Collision | None:
    collision = None
    for pair in itertools.combinations(spheres, 2):
        if pair[1].vel == pair[0].vel:
            continue
        relative_vel = pair[1].vel - pair[0].vel
        relative_pos = pair[1].pos - pair[0].pos
        rotated_pos = relative_pos.rotate(-relative_vel.heading)
        total_radius = sum(s.radius for s in pair)
        if total_radius < abs(rotated_pos.y) or rotated_pos.x > 0:
            continue
        dist_to_collision = -rotated_pos.x - math.sqrt(
            total_radius**2 - rotated_pos.y**2
        )
        try:
            time_to_collision = time_to_go(dist_to_collision, abs(relative_vel))
        except ValueError:
            continue  # Will never collide
        if collision is None or time_to_collision < collision.time_until:
            collision = Collision(time_to_collision, pair)
    return collision


def resolve(collision: Collision) -> float:
    pair = collision.participants
    if pair[0].mass == math.inf:
        pair = pair[::-1]
    collision_line = (pair[1].pos - pair[0].pos).heading
    rotated_vels = [p.vel.rotate(-collision_line) for p in pair]
    relative_velocity = rotated_vels[0].x - rotated_vels[1].x
    restitution = statistics.mean(p.restitution for p in pair)
    if pair[1].mass == math.inf:
        v1 = rotated_vels[1].x
    else:
        momentum = sum(p.mass * v.x for p, v in zip(pair, rotated_vels))
        v1 = (momentum + restitution * relative_velocity * pair[0].mass) / (
            pair[0].mass + pair[1].mass
        )
    v0 = v1 - restitution * relative_velocity
    v = (v0, v1)
    for i in range(2):
        pair[i].vel = Vec2(v[i], rotated_vels[i].y).rotate(collision_line)
    return relative_velocity
