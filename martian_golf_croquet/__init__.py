from __future__ import annotations

import itertools
import math
import random
import time
from dataclasses import dataclass

import pyglet
from pyglet.math import Vec2

from . import game, instructions, physics, world


def samples_config(samples: int | None) -> dict[str, int]:
    if samples is None:
        return {}
    return {"sample_buffers": 1, "samples": samples}


@dataclass
class Camera:
    old_pos: Vec2
    new_pos: Vec2
    old_time: float
    transition_time: float = 0.7

    def pan_to(self, pos: Vec2) -> None:
        self.old_pos = self.new_pos
        self.old_time = time.time()
        self.new_pos = pos

    def done(self):
        return time.time() - self.old_time > self.transition_time

    def pos(self) -> Vec2:
        progress = (time.time() - self.old_time) / self.transition_time
        if progress > 1:
            return self.new_pos
        linear_progress = progress**2 * (3 - 2 * progress)
        return self.old_pos * (1 - linear_progress) + self.new_pos * linear_progress


@dataclass
class Particle:
    start_pos: Vec2
    vel: Vec2
    colour: tuple[int, int, int]
    size: float
    lifespan: float = 1

    def __post_init__(self) -> None:
        self.creation_time = time.time()

    def setup_draw(self, batch: pyglet.graphics.Batch) -> None:
        ...

    def pos(self) -> Vec2:
        return self.start_pos + self.vel * (time.time() - self.creation_time)

    def draw(self, time_: float) -> None:
        new_opacity = round(255 * (1 - (time_ - self.creation_time) / self.lifespan))
        if self.shape.opacity != new_opacity:
            self.shape.opacity = new_opacity

    @property
    def dead(self):
        return self.shape.opacity <= 0


class TriangleParticle(Particle):
    def setup_draw(self, batch: pyglet.graphics.Batch) -> None:
        self.shape = pyglet.shapes.Triangle(
            0, 0, 0, 0, 0, 0, color=self.colour, batch=batch
        )

    def draw(self, time_: float) -> None:
        pos = self.pos()
        self.shape.x, self.shape.y = pos
        self.shape.x2, self.shape.y2 = pos + Vec2(
            self.size * math.sqrt(3) / 2, -self.size / 2
        )
        self.shape.x3, self.shape.y3 = pos + Vec2(
            self.size * math.sqrt(3) / 2, self.size / 2
        )
        super().draw(time_)


class CircleParticle(Particle):
    def setup_draw(self, batch: pyglet.graphics.Batch) -> None:
        self.shape = pyglet.shapes.Circle(
            0, 0, radius=self.size, color=self.colour, segments=8, batch=batch
        )

    def draw(self, time_: float) -> None:
        self.shape.position = self.pos()
        super().draw(time_)


@dataclass
class Text:
    text: str
    x: float
    y: float
    font_size: float = 0.1
    normal_colour: tuple[int, int, int, int] = (255, 255, 255, 255)
    hover_colour: tuple[int, int, int, int] = (200, 200, 200, 255)
    active_colour: tuple[int, int, int, int] = (170, 170, 170, 255)

    def setup_draw(self, scale: int, batch: pyglet.graphics.Batch) -> None:
        self.scale = scale
        self.label = pyglet.text.Label(
            self.text,
            font_size=self.font_size * scale,
            x=scale * self.x,
            y=scale * self.y,
            anchor_y="bottom",
            color=self.normal_colour,
            batch=batch,
        )
        self.store_size(scale)

    def store_size(self, scale):
        self.max_x = self.label.content_width / scale
        self.max_y = self.label.content_height / scale

    @property
    def visible(self) -> bool:
        return self.label.visible

    @visible.setter
    def visible(self, value: bool) -> None:
        self.label.visible = value


@dataclass
class TextButton(Text):
    def mouse_is_over(self, x: float, y: float) -> bool:
        return self.x < x < self.x + self.max_x and self.y < y < self.y + self.max_y

    def mouse_over(self, x: float, y: float, mouse_down: bool) -> bool:
        if self.mouse_is_over(x, y):
            if mouse_down:
                self.label.color = self.active_colour
            else:
                self.label.color = self.hover_colour
            return True
        else:
            self.label.color = self.normal_colour
            return False


@dataclass
class TextToggleButton(TextButton):
    other_text: str = ""

    def toggle(self, x: float, y: float) -> None:
        if self.mouse_is_over(x, y):
            self.text, self.other_text = self.other_text, self.text
            self.label.text = self.text
            self.store_size(self.scale)


CAMERA_START = Vec2(-3, 0)


class Window(pyglet.window.Window):
    def __init__(self) -> None:
        pyglet.clock.schedule_interval(self.update, 1 / 120)
        self.mouse_pos = Vec2(0, 0)
        self.camera = Camera(CAMERA_START, CAMERA_START, time.time())
        self.wind_direction = random.uniform(0, math.tau)
        pyglet.resource.path = ["martian_golf_croquet/sound"]
        pyglet.resource.reindex()
        self.noises = {
            noise: pyglet.resource.media(filename, streaming=False)
            for noise, filename in (
                ("ball", "ball_noise.wav"),
                ("hoop", "hoop_noise.wav"),
            )
        }
        self.music = pyglet.resource.media("farm.mp3", streaming=False)
        player = self.music.play()
        player.loop = True
        player.volume = 0.5
        self.particles: list[Particle] = []
        self.win_label = None
        self.scene = "title"
        self.mouse_down = False
        self.world_back = pyglet.graphics.Group(order=0)
        self.world_front = pyglet.graphics.Group(order=1)
        self.title_label = Text("Martian Golf Croquet", x=1, y=2.3, font_size=0.15)
        self.buttons = [
            TextButton("Play", x=1, y=1.6),
            TextButton("Instructions", x=1, y=1.3),
            TextButton("Quit", x=1, y=1),
        ]
        self.player_button_positions = ((1, 1.3), (1.8, 1.3), (1, 1), (1.8, 1))
        self.player_buttons = [
            TextToggleButton(x=x, y=y, text="Human", other_text="Robot")
            for x, y in self.player_button_positions
        ]
        self.handicap_label = Text("Handicap", x=0.2, y=0.7, font_size=0.1)
        self.handicap_value_labels = (
            Text("", x=1.3, y=0.7, font_size=0.1),
            Text("", x=2.1, y=0.7, font_size=0.1),
        )
        self.handicap_buttons = (
            TextButton("+", x=1, y=0.7),
            TextButton("+", x=1.8, y=0.7),
        )
        self.handicap = 0
        for samples in (16, 8, 4, 2, None):
            try:
                super().__init__(
                    width=1280,
                    height=720,
                    caption="Martian Golf Croquet",
                    config=pyglet.gl.Config(
                        aux_buffers=0,
                        depth_size=0,
                        stencil_size=0,
                        double_buffer=True,
                        **samples_config(samples),
                    ),
                    resizable=True,
                    vsync=False,
                )
            except pyglet.window.NoSuchConfigException:
                continue
            else:
                pyglet.gl.glClearColor(170 / 255, 110 / 255, 80 / 255, 1)
                self.new_game()
                return

    def new_game(self) -> None:
        self.scene = "title"
        self.mouse_down = False
        self.game = game.Game.new()
        self.camera.pan_to(CAMERA_START)
        self.camera.pan_to(CAMERA_START)

    @property
    def human_turn(self) -> bool:
        return self.humans[self.game.turn]

    def play(self) -> None:
        self.scene = "game"
        self.game.points[self.handicap > 0] = abs(self.handicap)
        self.setup_draw()

    def pan_camera_to_turn(self) -> None:
        if self.game.target_hoop >= len(self.game.world.hoops):
            return
        turn_weight = 0.5
        hoop_weight = 0.3
        weights = [(1 - turn_weight - hoop_weight) / 3 for _ in range(4)]
        weights[self.game.turn] = turn_weight
        self.camera.pan_to(
            sum(b.pos * w for b, w in zip(self.game.world.balls, weights))
            + self.game.current_hoop.pos * (hoop_weight)
        )

    def play_sound_at(self, sound, volume: float, position: Vec2) -> None:
        noise = sound.play()
        noise.position = (*(position - self.camera.pos()) * 0.1, -0.5)
        noise.volume = volume

    def create_confetti(self, pos: Vec2) -> None:
        new_particles = [
            TriangleParticle(
                pos,
                vel=Vec2.from_polar(random.gauss(8, 3), random.uniform(0, math.tau)),
                size=random.gauss(0.04, 0.005),
                colour=tuple(random.randrange(200, 256) for _ in range(3)),
                lifespan=random.gauss(1, 0.1),
            )
            for _ in range(150)
        ]
        for p in new_particles:
            p.setup_draw(self.world_batch)
        self.particles += new_particles

    def create_sand(self, dt: float) -> None:
        self.wind_direction += random.gauss(0, dt / 3)
        if random.random() < 5 * dt:
            vel = Vec2.from_polar(
                random.gauss(0.8, 0.1), self.wind_direction + random.gauss(0, 0.01)
            )
            self.particles.append(
                CircleParticle(
                    self.camera.pos()
                    - vel * 6
                    + Vec2(random.uniform(-3, 3), random.uniform(-3, 3)),
                    vel=vel,
                    colour=(190, 150, 80),
                    size=random.gauss(0.01, 0.001),
                    lifespan=8,
                )
            )
            self.particles[-1].setup_draw(self.world_batch)

    def update(self, dt: float) -> None:
        self.create_sand(dt)
        cursor = self.CURSOR_DEFAULT
        if self.scene in {"title", "play"}:
            if any(
                button.mouse_over(
                    self.mouse_pos.x / self.scale,
                    self.mouse_pos.y / self.scale,
                    self.mouse_down,
                )
                for button in (
                    self.buttons
                    if self.scene == "title"
                    else (self.buttons[0], *self.player_buttons, *self.handicap_buttons)
                )
            ):
                cursor = self.CURSOR_HAND
        elif self.scene == "game" and self.game.can_go and self.human_turn:
            cursor = self.CURSOR_CROSSHAIR
        self.set_mouse_cursor(self.get_system_mouse_cursor(cursor))
        if (
            self.scene == "game"
            and self.game.can_go
            and not self.human_turn
            and self.camera.done()
        ):
            self.hit(self.game.get_ai_move())
        if self.scene not in {"game", "win"}:
            return
        events = self.game.do_collision()
        if "done" in events:
            self.pan_camera_to_turn()
        for event in events:
            if isinstance(event, physics.Collision):
                assert event.impact_speed is not None
                self.play_sound_at(
                    self.noises["ball"],
                    self.volume(event.impact_speed),
                    (event.participants[0].pos + event.participants[1].pos) * 0.5,
                )
            if isinstance(event, world.HoopRun):
                hoop_pos = self.game.world.hoops[event.hoop].pos
                self.play_sound_at(self.noises["hoop"], 0.25, hoop_pos)
                self.create_confetti(hoop_pos)
                self.win()

    def win(self) -> None:
        try:
            won = self.game.points.index(5 + abs(self.handicap) // 2)
        except ValueError:
            return
        for ball in self.game.world.balls[won::2]:
            self.create_confetti(ball.pos)
            team = " and ".join(world.BALL_NAMES[won::2])
            self.win_label = pyglet.text.Label(
                f"{team} win!",
                font_size=0.1 * self.scale,
                x=self.width // 2,
                y=self.height // 2,
                anchor_x="center",
                anchor_y="center",
                color=(255, 255, 255, 255),
                batch=self.ui_batch,
            )
            self.scene = "win"
            self.game.can_go = False

    def prepare_draw_scaled(self) -> None:
        size = Vec2(self.width, self.height) * (1 / self.scale)
        bottom_left = self.camera.pos() - size * (0.5)
        top_right = self.camera.pos() + size * (0.5)
        self.projection = pyglet.math.Mat4.orthogonal_projection(
            bottom_left.x, top_right.x, bottom_left.y, top_right.y, -1, 1
        )

    def prepare_draw_ui(self) -> None:
        self.projection = pyglet.math.Mat4.orthogonal_projection(
            0, self.width, 0, self.height, -1, 1
        )

    def on_resize(self, width: int, height: int) -> None:
        self.scale = round(min(height, width * 9 / 16) / 3)
        self.screen_centre = Vec2(self.width / 2, self.height / 2)
        self.setup_draw()
        self.viewport = (0, 0, width, height)

    def on_mouse_motion(self, x: int, y: int, *_) -> None:
        self.mouse_pos = Vec2(x, y)

    on_mouse_drag = on_mouse_motion

    @property
    def aim(self) -> Vec2:
        return self.game.clamp_aim(
            (self.mouse_pos - self.screen_centre) / self.scale
            + self.camera.pos()
            - self.game.current_ball.pos
        )

    @staticmethod
    def volume(speed: float) -> float:
        return speed**2 / 30

    def on_key_press(self, symbol, _):
        if symbol == pyglet.window.key.ESCAPE:
            if self.scene == "title":
                self.close()
                return
            elif self.scene in {"play", "instructions"}:
                self.scene = "title"
            elif self.scene in {"game", "win"}:
                self.new_game()
            self.setup_draw()

    def on_mouse_press(self, *_) -> None:
        self.mouse_down = True

    def show_handicap(self):
        text = ["", ""]
        if self.handicap > 0:
            text[1] = f"+{self.handicap}"
        elif self.handicap < 0:
            text[0] = f"+{-self.handicap}"
        for t, b in zip(text, self.handicap_value_labels):
            b.label.text = t

    def on_mouse_release(self, x: float, y: float, *_) -> None:
        scaled = (x / self.scale, y / self.scale)
        self.mouse_down = False
        if self.scene == "win":
            self.new_game()
            self.setup_draw()
            return
        if self.scene == "instructions":
            self.scene = "title"
            self.setup_draw()
            return
        if self.scene == "title":
            for button in self.buttons:
                if button.mouse_is_over(*scaled):
                    if button.text == "Play":
                        self.scene = "play"
                        self.setup_draw()
                        self.show_handicap()
                    elif button.text == "Instructions":
                        self.scene = "instructions"
                        self.setup_draw()
                    elif button.text == "Quit":
                        self.close()
            return
        if self.scene == "play":
            for button in self.player_buttons:
                button.toggle(*scaled)
            if self.buttons[0].mouse_is_over(*scaled):
                self.humans = [p.text == "Human" for p in self.player_buttons]
                self.play()
            for i, button in enumerate(self.handicap_buttons):
                if button.mouse_is_over(*scaled):
                    self.handicap += 4 * i - 2
                    self.handicap = min(max(self.handicap, -8), 8)
                    self.show_handicap()

        if self.game.can_go:
            self.hit(self.aim)

    def hit(self, aim: Vec2) -> None:
        aim = aim.rotate(random.gauss(0, self.accuracy)) * (
            physics.RESISTANCE * random.gauss(1, 0.05)
        )
        self.play_sound_at(
            self.noises["ball"], self.volume(abs(aim)), self.game.current_ball.pos
        )
        self.game.hit(aim)

    @property
    def accuracy(self) -> float:
        return 0.02 / (2.2 - abs(self.aim))

    def setup_draw(self) -> None:
        self.world_batch = pyglet.graphics.Batch()
        self.ui_batch = pyglet.graphics.Batch()
        self.instructions = [
            pyglet.text.Label(
                part,
                multiline=True,
                width=self.scale * 2.3,
                x=self.scale * x,
                y=self.scale * 2.8,
                batch=self.ui_batch,
                font_size=self.scale * 0.08,
            )
            for x, part in zip((0.2, 0.2 + 8 / 3), instructions.instructions)
        ]
        self.ball_button_indicators = [
            pyglet.shapes.Circle(
                x=(x - 0.07) * self.scale,
                y=(y + 0.09) * self.scale,
                radius=world.BALL_RADIUS * self.scale,
                color=c,
                batch=self.ui_batch,
            )
            for (x, y), c in zip(self.player_button_positions, world.BALL_COLOURS)
        ]
        for label in (
            self.title_label,
            self.handicap_label,
            *self.handicap_value_labels,
            *self.handicap_buttons,
            *self.buttons,
            *self.player_buttons,
        ):
            label.setup_draw(self.scale, self.ui_batch)

        self.score_colour_balls: list[pyglet.Shape.Circle] = []
        for team in range(2):
            self.score_colour_balls.extend(
                pyglet.shapes.Circle(
                    x=(0.08 + 0.25 * team) * self.scale,
                    y=(0.06 + 0.06 * o) * self.scale,
                    radius=0.04 * self.scale,
                    color=col,
                    batch=self.ui_batch,
                    segments=30,
                )
                for o, col in enumerate(world.BALL_COLOURS[team + 2 :: -2])
            )
        self.score_labels = [
            pyglet.text.Label(
                text=str(self.game.points[team]),
                font_size=0.1 * self.scale,
                x=(0.15 + 0.25 * team) * self.scale,
                y=0.05 * self.scale,
                batch=self.ui_batch,
            )
            for team in range(2)
        ]
        for thing in itertools.chain(self.score_labels, self.score_colour_balls):
            thing.visible = self.scene in {"game", "win"}

        for thing in (self.title_label, *self.buttons):
            thing.visible = self.scene == "title"

        for i in self.instructions:
            i.visible = self.scene == "instructions"

        for thing in itertools.chain(
            self.player_buttons,
            self.ball_button_indicators,
            (self.handicap_label,),
            self.handicap_value_labels,
            self.handicap_buttons,
        ):
            thing.visible = self.scene == "play"

        self.buttons[0].visible = self.scene in {"title", "play"}

        self.go_highlight = pyglet.shapes.Circle(
            0,
            0,
            world.BALL_RADIUS * 1.2,
            color=(255, 255, 255),
            batch=self.world_batch,
            group=self.world_back,
            segments=30,
        )
        self.aim_sector = pyglet.shapes.Sector(
            0,
            0,
            1,
            color=(255, 255, 255),
            batch=self.world_batch,
            group=self.world_front,
        )
        self.aim_sector.opacity = 100

        self.game.world.setup_draw(self.world_batch, self.world_back)
        for particle in self.particles:
            particle.setup_draw(self.world_batch)

    def on_draw(self) -> None:
        self.clear()
        self.prepare_draw_scaled()
        human_can_go = self.game.can_go and self.human_turn and self.scene == "game"
        self.go_highlight.visible = human_can_go
        self.aim_sector.visible = human_can_go
        if human_can_go:
            self.go_highlight.position = self.game.current_ball.pos
            self.aim_sector.position = self.game.current_ball.pos
            self.aim_sector.radius = abs(self.aim)
            self.aim_sector.start_angle = self.aim.heading - 2 * self.accuracy
            self.aim_sector.angle = 4 * self.accuracy

        self.game.world.draw(self.game.target_hoop, self.world_batch, self.world_back)
        time_ = time.time()
        for particle in self.particles:
            particle.draw(time_)
        self.particles = [p for p in self.particles if not p.dead]
        self.world_batch.draw()
        self.prepare_draw_ui()
        for score, label in zip(self.game.points, self.score_labels):
            label.text = str(score)
        self.ui_batch.draw()


def main() -> None:
    Window()
    pyglet.app.run()
