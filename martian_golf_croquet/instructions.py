text = """
On Earth, golf was perhaps the most popular of the accuracy-based ball games.
This state of affairs was unlikely to persist following the colonisation of Mars,
as the planet’s surface is more-or-less a giant bunker.
Despite stiff competition,
the venerable game of golf croquet succeeded in claiming the crown.
Martian culture rejected the traditional six-hoop formation Earth so enjoys.
Just stick some hoops down amongst the rocks and let’s play!

There are two teams: blue & black v. red & yellow.
Your team must hit its balls through (‘run’) the most hoops to win.
The balls are played in sequence
(blue, red, black, yellow
– the score indicator in the bottom-left may be helpful for remembering this).
----
Hoops must be run from left to right.
The current target hoop has a green marker.
When someone runs it, they gain a point for their team,
and the next hoop becomes the target.

Use the mouse to aim and hit the ball.
The width of the aim sector shows how accurate your shot will be;
95 % of the time, the angle will be inside it.

You can play against your friends or the station robots.
The station robots were designed for mopping,
so they are not very good at croquet.
It is usual to give them a generous handicap.

The offside rule never made it to Mars.
"""
instructions = text.strip().replace("\n\n", "\u2029\t").replace("\n", " ").split("----")
