from __future__ import annotations

import math
import time
from dataclasses import dataclass

from pyglet.math import Vec2

from . import physics, world


@dataclass
class Game:
    world: world.World
    turn: int
    target_hoop: int
    points: list[int]
    can_go: bool

    @classmethod
    def new(cls):
        return cls(world.World.new(), 0, 0, [0, 0], False)

    def update_balls(self, time: float) -> None:
        self.world.advance_time(time)

    def next_turn(self) -> None:
        self.turn += 1
        self.turn %= 4

    @property
    def current_ball(self):
        return self.world.balls[self.turn]

    @property
    def current_hoop(self):
        return self.world.hoops[self.target_hoop]

    def simulate(self) -> None:
        self.can_go = False
        self.world.simulate()

    def do_collision(self) -> list[str | physics.Event]:
        if self.can_go:
            return []
        self.world.current_time = time.time()
        if self.world.event is None:
            if self.world.all_slow():
                self.update_balls(math.inf)
                self.can_go = True
                self.world.add_ball()
                return ["done"]
            return []

        result: list[str | physics.Event] = []
        while self.world.has_collided():
            if (
                isinstance(self.world.event, world.HoopRun)
                and self.world.event.hoop == self.target_hoop
            ):
                self.points[self.world.event.ball % 2] += 1
                self.target_hoop += 1
                result.append(self.world.event)
            self.update_balls(self.world.event.time_until)
            if isinstance(self.world.event, physics.Collision):
                self.world.event.impact_speed = physics.resolve(self.world.event)
                result.append(self.world.event)
            else:
                assert isinstance(self.world.event, world.BaseHoopRun)
                self.world.event.apply(self.world.balls)

            self.world.last_time += self.world.event.time_until
            self.world.collide()
        return result

    def hit(self, aim: Vec2) -> None:
        self.current_ball.vel = aim
        self.next_turn()
        self.simulate()

    def clamp_aim(self, aim: Vec2) -> Vec2:
        if abs(aim) > 2:
            return aim.from_magnitude(2)
        return aim

    def clamp_ai_aim(self, aim: Vec2) -> Vec2:
        if abs(aim) > 1.9:
            return aim.from_magnitude(1.9)
        return aim

    def get_ai_move(self) -> Vec2:
        target_vector = self.current_hoop.pos - self.current_ball.pos
        if self.current_ball.running == self.target_hoop:
            return Vec2.from_polar(1, self.current_hoop.angle)
        if (
            abs(
                (target_vector.heading - self.current_hoop.angle + math.pi) % math.tau
                - math.pi
            )
            < 0.5
        ):
            return self.clamp_ai_aim(
                target_vector.from_magnitude(0.2 + abs(target_vector))
            )
        return self.clamp_ai_aim(
            target_vector - Vec2.from_polar(0.2, self.current_hoop.angle)
        )
