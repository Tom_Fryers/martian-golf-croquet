from __future__ import annotations

import itertools
import math
import random
import time
from collections.abc import Sequence
from dataclasses import dataclass

import pyglet
from pyglet.math import Vec2

from . import physics


@dataclass
class DrawableCircle(physics.Circle):
    colour: tuple[int, int, int] = (255, 255, 255)

    def setup_draw(
        self, batch: pyglet.graphics.Batch, group: pyglet.graphics.Group
    ) -> None:
        self.shape = pyglet.shapes.Circle(
            0,
            0,
            self.radius,
            color=self.colour,
            batch=batch,
            group=group,
            segments=round(5 * math.sqrt(self.radius * 300 + 1)),
        )

    def draw(self, time_: float) -> None:
        new_pos = self.pos_in(time_)
        if new_pos != self.shape.position:
            self.shape.position = new_pos


BALL_RADIUS = 0.0460375


class Ball(DrawableCircle):
    def __init__(self, **kwargs) -> None:
        self.running: int | None = None
        super().__init__(
            mass=0.45359237, radius=BALL_RADIUS, restitution=0.752, **kwargs
        )


HALF_HOOP_WIDTH = 0.060325
HOOP_WIRE_RADIUS = 0.009525
HALF_PAINT_LENGTH = 0.03


HOOP_RUN_OFFSET = BALL_RADIUS - HOOP_WIRE_RADIUS


@dataclass
class BaseHoopRun(physics.Event):
    hoop: int
    ball: int

    def applicable(self, ball: Ball) -> bool:
        ...

    def apply(self, balls: Sequence[Ball]) -> None:
        self._apply(balls[self.ball])

    def _apply(self, ball: Ball) -> None:
        ...


class HoopStartRun(BaseHoopRun):
    def _apply(self, ball: Ball) -> None:
        ball.running = self.hoop

    def applicable(self, ball: Ball) -> bool:
        return ball.running is None


class HoopRun(BaseHoopRun):
    def _apply(self, ball: Ball) -> None:
        ball.running = None

    def applicable(self, ball: Ball) -> bool:
        return ball.running == self.hoop


class HoopStopRun(BaseHoopRun):
    def _apply(self, ball: Ball) -> None:
        ball.running = None

    def applicable(self, ball: Ball) -> bool:
        return ball.running == self.hoop


@dataclass
class Hoop:
    pos: Vec2
    angle: float
    colour: tuple[int, int, int] = (255, 255, 255)

    def __post_init__(self) -> None:
        self.ends = [
            self.pos
            + Vec2.from_polar(1, self.angle - math.pi / 2) * (d * HALF_HOOP_WIDTH)
            for d in (-1, 1)
        ]
        self.circles = [
            DrawableCircle(
                colour=self.colour,
                radius=HOOP_WIRE_RADIUS,
                mass=math.inf,
                restitution=0.01,
                pos=e,
            )
            for e in self.ends
        ]

    def in_own_coordinates(self, pos: Vec2) -> Vec2:
        return (pos - self.pos).rotate(-self.angle)

    def runs(
        self, old_pos: Vec2, new_pos: Vec2, edge: int, direction: int = 1
    ) -> float | None:
        old_pos, new_pos = (
            (self.in_own_coordinates(p) - Vec2(edge * HOOP_RUN_OFFSET, 0)) * direction
            for p in (old_pos, new_pos)
        )
        if (
            old_pos.x < 0 < new_pos.x
            and abs(
                old_pos.y
                - (new_pos.y - old_pos.y) / (new_pos.x - old_pos.x) * old_pos.x
            )
            < HALF_HOOP_WIDTH
        ):
            return abs(new_pos - old_pos) * (-old_pos.x / (new_pos.x - old_pos.x))
        return None


BALL_COLOURS = ((40, 90, 210), (210, 30, 40), (0, 0, 0), (230, 220, 20))

BALL_NAMES = ("Blue", "Red", "Black", "Yellow")


def random_trapezoidal(
    min_: float, min_uniform: float, max_uniform: float, max_: float
) -> float:
    return (random.uniform(min_, max_uniform) + random.uniform(min_uniform, max_)) / 2


@dataclass
class World:
    balls: list[Ball]
    hoops: list[Hoop]
    obstacles: list[DrawableCircle]
    event: physics.Event | None = None
    last_time: float = 0
    current_time: float = 0
    balls_in: int = 0

    @classmethod
    def new(cls):
        hoop_pos = Vec2(1, 0)
        hoops = []
        for _ in range(9):
            angle = min(max(random.gauss(0, 0.5), -1.5), 1.5)
            hoops.append(Hoop(hoop_pos, angle))
            hoop_pos += Vec2(random.gauss(1, 0.1), random.gauss(-0.1 * hoop_pos.y, 0.8))

        def random_pos() -> Vec2:
            return Vec2(
                random_trapezoidal(-7, -6, 13, 15), random_trapezoidal(-7, -6, 6, 7)
            )

        obstacles: list[DrawableCircle] = []

        def is_ok_for_obstacle(pos: Vec2, radius: float) -> bool:
            return (
                all(
                    pos.distance(h.pos)
                    > radius
                    + max(
                        HALF_HOOP_WIDTH + HOOP_WIRE_RADIUS,
                        1.1 * (2 * BALL_RADIUS + HOOP_WIRE_RADIUS),
                    )
                    for h in hoops
                )
                and all(pos.distance(r.pos) > radius + r.radius for r in obstacles)
                and pos.distance(Vec2(0, 0)) > radius + BALL_RADIUS * 4
            )

        for _ in range(1000):
            radius = random.lognormvariate(-1.5, 0.5)
            if radius > 1:
                continue
            mass = 4 / 3 * math.pi * 2900 * radius**3

            if random.random() < 10 / mass:
                while True:
                    pos = random_pos()
                    if is_ok_for_obstacle(pos, radius):
                        break
                obstacles.append(
                    DrawableCircle(
                        radius=radius,
                        mass=mass,
                        restitution=0.6,
                        pos=pos,
                        colour=(160, 140, 120),
                    )
                )

        return cls(
            [Ball(pos=Vec2(0, 0), colour=c) for c in BALL_COLOURS], hoops, obstacles
        )

    def setup_draw(
        self, batch: pyglet.graphics.Batch, group: pyglet.graphics.Group
    ) -> None:
        for circle in self.all_circles(hidden_balls=True):
            circle.setup_draw(batch, group)
        for ball in self.balls[self.balls_in :]:
            ball.shape.visible = False

        self.hoop_shapes = [
            pyglet.shapes.Line(
                *hoop.ends[0],
                *hoop.ends[1],
                HOOP_WIRE_RADIUS * 2,
                color=hoop.colour,
                batch=batch,
                group=group,
            )
            for hoop in self.hoops
        ]

        self.next_hoop_indicator = pyglet.shapes.Line(
            0,
            0,
            0,
            0,
            HOOP_WIRE_RADIUS * 2,
            color=(20, 100, 40),
            batch=batch,
            group=group,
        )

    def draw(
        self,
        target_hoop: int,
        batch: pyglet.graphics.Batch,
        group: pyglet.graphics.Group,
    ) -> None:
        time_ = self.time_since_last()
        for circle in self.all_circles():
            circle.draw(time_)
        if target_hoop < len(self.hoops):
            hoop = self.hoops[target_hoop]
            half_hoop_paint = Vec2.from_polar(
                HALF_PAINT_LENGTH, hoop.angle + math.pi / 2
            )
            self.next_hoop_indicator.x, self.next_hoop_indicator.y = (
                hoop.pos + half_hoop_paint
            )
            self.next_hoop_indicator.x2, self.next_hoop_indicator.y2 = (
                hoop.pos - half_hoop_paint
            )

    def advance_time(self, time_: float) -> None:
        for thing in itertools.chain(self.balls, self.obstacles):
            thing.update(time_)

    def all_circles(self, hidden_balls: bool = False) -> list[DrawableCircle]:
        return list(
            itertools.chain(
                self.balls[: 4 if hidden_balls else self.balls_in],
                itertools.chain.from_iterable(h.circles for h in self.hoops),
                self.obstacles,
            )
        )

    def get_next_hoop_run(self) -> BaseHoopRun | None:
        runs = []
        for b, ball in enumerate(self.balls):
            new_pos = ball.pos_in(math.inf)
            for h, hoop in enumerate(self.hoops):
                for side, direction, type_ in (
                    (-1, 1, HoopStartRun),
                    (1, 1, HoopRun),
                    (-1, -1, HoopStopRun),
                ):
                    dist = hoop.runs(ball.pos, new_pos, side, direction)
                    if dist is not None:
                        run = type_(physics.time_to_go(dist, abs(ball.vel)), h, b)
                        if run.applicable(ball):
                            runs.append(run)
        return physics.soonest(runs)

    def collide(self) -> None:
        hoop_run = self.get_next_hoop_run()
        collision = physics.find_first_collision(self.all_circles())
        if hoop_run is None:
            self.event = collision
            return
        if collision is None:
            self.event = hoop_run
            return

        self.event = physics.soonest((hoop_run, collision))

    def simulate(self) -> None:
        self.collide()
        self.last_time = time.time()
        self.current_time = self.last_time

    def time_since_last(self) -> float:
        return self.current_time - self.last_time

    def has_collided(self) -> bool:
        return self.event is not None and self.time_since_last() > self.event.time_until

    def all_slow(self, threshold: float = 0.001) -> bool:
        return max(abs(b.vel_in(self.time_since_last())) for b in self.balls) < 0.001

    def add_ball(self) -> None:
        if self.balls_in < 4:
            ball = self.balls[self.balls_in]
            ball.shape.visible = True
            blockers = self.balls[: self.balls_in]
            possible_points = [Vec2(0, 0)]
            possible_points += [
                -b.pos.from_magnitude(b.radius + ball.radius - abs(b.pos) + 0.001)
                for b in blockers
            ]
            for b, c in itertools.combinations(blockers, 2):
                midpoint = (b.pos + c.pos) * 0.5
                parallel = c.pos - b.pos
                if abs(parallel) > b.radius + c.radius + ball.radius * 2:
                    continue
                offset = Vec2.from_polar(
                    math.sqrt((b.radius + ball.radius) ** 2 - (abs(parallel) / 2) ** 2)
                    + 0.001,
                    parallel.heading + math.pi / 2,
                )
                possible_points += [midpoint + offset, midpoint - offset]
            possible_points.sort(key=abs)
            ball.pos = next(
                p
                for p in possible_points
                if all(b.pos.distance(p) >= b.radius + ball.radius for b in blockers)
            )
            self.balls_in += 1
