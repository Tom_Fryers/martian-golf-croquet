# Martian Golf Croquet

A relaxing game of golf croquet, on Mars. There are instructions in
game, and it should be easy to pick up from playing. If you are
confused, [a refresher on Earth golf
croquet](https://www.croquet.org.uk/?p=games/golf/gcSynopsis) might
help.

## Dependencies

- Python 3.8+
- pyglet 1.5.23+

## Credits

I wrote the code, for the game, and ‘drew’ the ‘artwork’. I also created
the sound effects myself. (The ball clacking is real, slightly edited;
the hoop noise was made with [jsfxr](https://sfxr.me).)

The music is

"Farm"
Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 4.0 License
http://creativecommons.org/licenses/by/4.0/
